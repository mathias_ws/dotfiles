local wezterm = require("wezterm")

local config = wezterm.config_builder()

config.window_background_opacity = 0.90

config.color_schemes = {
	["Deep"] = {
		foreground = "#93a4c3",
		background = "#1a212e",
		cursor_bg = "#c7c7c7",
		cursor_border = "#93a4c3",
		cursor_fg = "#feffff",
		selection_bg = "#96a3c0",
		selection_fg = "#000000",
		brights = {
			"#0c0d14", -- black
			"#e36369", -- red
			"#99cb69", -- green (diff_add)
			"#e7be6c", -- yellow
			"#5ea4f5", -- blue (diff_text)
			"#b960e0", -- purple
			"#61bccd", -- cyan
			"#6f7c99", -- light_grey
		},
	},
}

config.color_scheme = "Deep"

config.keys = {
	{
		-- Tab opens in home dir.
		key = "t",
		mods = "CMD",
		action = wezterm.action.SpawnCommandInNewTab({
			cwd = wezterm.home_dir,
		}),
	},
}

config.send_composed_key_when_left_alt_is_pressed = true
config.send_composed_key_when_right_alt_is_pressed = true
config.window_close_confirmation = "NeverPrompt"
config.window_decorations = "RESIZE"
config.enable_tab_bar = false

return config
